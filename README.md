# WHMCS Webnode Module #


## For Users ##
### How to setup ###
1. Go to https://gitlab.com/w168/whmcs-module/-/releases
2. Download "Released ZIP file" from Assets->other
3. Add "webnode" folder from sitebuilder-x.x.x/addon to modules/addons
4. Add "webnode" folder from sitebuilder-x.x.x/provisioning to modules/servers
5. Configure "Webnode Sitebuilder" Addon (configuration example bellow is for production, whre Client ID and Secret will be provided by webnode per partner)
   1. OAuth Url: https://oauth2.webnode.com/
   2. Api Url: https://api-gateway.webnode.com/
   3. Client Id: [...]
   4. Client Secret: [...]
6. Activate "Webnode Sitebuilder"
7. Activate/Setup Webnode provisioning module "Team.blue Webnode"
8. (*This Step Is currently optional, but may not be in future*) Setup cron.php such that it's called sufficiently enough that any event created by project creation can be consumed within a minute
   * calling cron:  `php cron.php "client_id" "client_secret" "oAuth2Url" "apiGatewayUrl" "count of events to consume from queue per call"`
9. Create product/service and in Module setings set package to one of: limited/mini/standard/professional/business

# For Developers #
## How to release ##
1. Create tag
2. Run pipeline with variable RELEASE_VERSION=x.x.x (x.x.x = your release version)
3. After tests are finished start pre-release-upload job
4. Create Release for created tag with asset with link:
   1. URL: printed by pre-release-upload job
   2. Link title: "Released ZIP file"
   3. Type: Other

## Tests ##

1. copy `tests/integration/.example.env` to `tests/integration/.env` and fill in public+secret key
2. composer install 
3. composer test-integration

