#!/bin/bash

rm -rf __dist
mkdir -p __dist/provisioning/webnode
mkdir -p __dist/addon/webnode

cd provisioning

composer install --no-dev
composer dump -a

cp -ar src ../__dist/provisioning/webnode
cp -ar templates ../__dist/provisioning/webnode
cp -ar vendor ../__dist/provisioning/webnode
cp composer.json ../__dist/provisioning/webnode
cp logo.png ../__dist/provisioning/webnode
cp webnode.php ../__dist/provisioning/webnode
cp cron.php ../__dist/provisioning/webnode
cp whmcs.json ../__dist/provisioning/webnode

composer install


cd ../addon
cp webnode.php ../__dist/addon/webnode


