<?php
return [
	'servers' => [
		'oauth2' => 'https://oauth2.webnode.com',
		'api' => 'https://api-gateway.webnode.com',
	],
];