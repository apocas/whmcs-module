<?php

namespace wnd\whmcs\HttpClient;

use wnd\whmcs\HttpClient\HttpRequestException;
use wnd\whmcs\HttpClient\Response;

/**
 * Simplest possible client so we don't have any prod dependencies and it's easier to suport multiple versions of whmcs
 */
class Client
{
	public const TYPE_GET = 'GET';
	public const TYPE_POST = 'POST';
	public const TYPE_PATCH = 'PATCH';
	public const TYPE_DELETE = 'DELETE';

	/** @var string */
	private $baseUrl;

	/** @var bool */
	private $verify;

	/**
	 * @param string $baseUrl
	 * @param bool $verify
	 */
	public function __construct(string $baseUrl, bool $verify = true)
	{
		$this->baseUrl = $baseUrl;
		$this->verify = $verify;
	}

	/**
	 * @param string $method
	 * @param string $url
	 * @param array<string, mixed> $data
	 * @param array<int, mixed> $headers
	 * @return Response
	 */
	public function request(string $method, string $url, array $data = [], array $headers = []): Response
	{
		return $this->curlRequest($method, $this->getUrl($url), $data, $headers);
	}

	private function getUrl(string $url): string
	{
		if (empty($this->baseUrl))
		{
			return $url;
		}
		return rtrim($this->baseUrl, '/') . '/' . ltrim($url, '/');
	}

	/**
	 * Common curl request method for GET and POST request (credits to from https://github.com/haruncpi/http/blob/master/src/Http.php#L48)
	 *
	 * @param string    $type          HTTP verb GET, POST.
	 * @param string    $url           Request URL.
	 * @param array<string, mixed>     $data          Request data.
	 * @param array<int, mixed>     $headers       Headers data.
	 * @return Response
	 */
	private function curlRequest(string $type, string $url, array $data = [], array $headers = []): Response
	{
		$curl = curl_init();
		$type = strtoupper($type);

		if (self::TYPE_GET === $type && count($data)) {
			$url = $url . '?' . http_build_query($data);
		}

		$options = [
			CURLOPT_URL             => $url,
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_ENCODING        => "",
			CURLOPT_MAXREDIRS       => 10,
			CURLOPT_TIMEOUT         => 30,
			CURLOPT_CUSTOMREQUEST   => $type,
			CURLOPT_HTTPHEADER      => $headers,
		];

		if (!$this->verify)
		{
			$options[CURLOPT_SSL_VERIFYHOST] = 0;
			$options[CURLOPT_SSL_VERIFYPEER] = 0;
		}


		// Must required for get headers and body
		$options[CURLOPT_HEADER] = 1;

		if (in_array($type, [self::TYPE_POST, self::TYPE_PATCH, self::TYPE_DELETE]) && count($data)) {
			$options[CURLOPT_HTTPHEADER][] = 'Content-Type:application/json';
			$options[CURLOPT_POSTFIELDS] = json_encode($data);
		}

		curl_setopt_array($curl, $options);

		$response = curl_exec($curl);
		$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

		if (!is_string($response))
		{
			throw new HttpRequestException('Request failed ' . $statusCode);
		}

		$resonseHeaders = substr($response, 0, $header_size);
		$body = substr($response, $header_size);

		curl_close($curl);

		return new Response($body, (int)$statusCode, $resonseHeaders);
	}
}
