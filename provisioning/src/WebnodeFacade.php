<?php

declare(strict_types=1);

namespace wnd\whmcs;

use InvalidArgumentException;
use RuntimeException;
use wnd\whmcs\ApiClient\ApiAuthorizationService;
use wnd\whmcs\ApiClient\ApiProjectCreateClient;
use wnd\whmcs\ApiClient\ApiReadClient;
use wnd\whmcs\ApiClient\ApiUpdateProjectClient;
use wnd\whmcs\ApiClient\WebnodeOAuth2Service;

final class WebnodeFacade
{
	/** @var ApiReadClient */
	private $readClient;

	/** @var ApiProjectCreateClient */
	private $projectCreateClient;

	/** @var ApiUpdateProjectClient */
	private $updateProjectClient;

	/** @var WebnodeOAuth2Service */
	private $oAuth2Api;

	/** @var ApiAuthorizationService */
	private $authorizationService;

	public function __construct(
		ApiReadClient $readClient,
		ApiProjectCreateClient $projectCreateClient,
		ApiUpdateProjectClient $updateProjectClient,
		WebnodeOAuth2Service $oAuth2Api,
		ApiAuthorizationService $authorizationService
	) {
		$this->readClient = $readClient;
		$this->projectCreateClient = $projectCreateClient;
		$this->updateProjectClient = $updateProjectClient;
		$this->oAuth2Api = $oAuth2Api;
		$this->authorizationService = $authorizationService;
	}

	public function clean(): void
	{
		$this->authorizationService->revokeAccessToken();
	}

	/**
	 * @param string $userIdentifier
	 * @return  array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      }
	 * }[]|null
	 * @throws InvalidArgumentException
	 */
	public function findByUserIdentifier(string $userIdentifier): ?array
	{
		return $this->readClient->findByUserIdentifier($userIdentifier);
	}

	/**
	 * @param string $identifier
	 * @return array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      },
	 *     installationCompleted: bool
	 * }|null
	 */
	public function findByIdentifier(string $identifier): ?array
	{
		return $this->readClient->findByIdentifier($identifier);
	}

	/**
	 * @return array{
	 *      domainName: string,
	 *      isHttps: bool,
	 *      createDate: string,
	 *      isMasterDomain: bool,
	 *      projectIdentifier:string,
	 *      userIdentifier: string
	 * }[]
	 */
	public function getProjectDomains(string $identifier): array
	{
		return $this->readClient->getProjectDomains($identifier);
	}

	/**
	 * @return array{identifier:string, isActive: bool}[]
	 */
	public function getProjectPackages(string $identifier): array
	{
		return $this->readClient->getProjectPackages($identifier);
	}

	public function deleteProject(string $identifier): void
	{
		$this->updateProjectClient->deleteProject($identifier);
	}

	/**
	 * For 30 days after project deletion, you are able to restore it
	 * @param string $identifier
	 * @return void
	 */
	public function restoreSoftDeletedProject(string $identifier): void
	{
		$this->updateProjectClient->restoreSoftDeletedProject($identifier);
	}

	/**
	 * @throws RuntimeException
	 */
	public function setProjectEnabled(string $identifier, bool $enabled): void
	{
		$this->updateProjectClient->setProjectEnabled($identifier, $enabled);
	}


	public function create(string $userEmail, string $identifier = null): NewProjectResponse
	{
		return $this->projectCreateClient->create($userEmail, $identifier);
	}

	/**
	 * @param string $identifier
	 * @param int $timeout (in ms)
	 * @param int $step (in ms)
	 * @return void
	 * @throws RuntimeException
	 */
	public function waitForProjectReady(string $identifier, int $timeout = 60000, int $step = 250): void
	{
		$this->projectCreateClient->waitForProjectReady($identifier, $timeout, $step);
	}

	/**
	 * @param string $identifier
	 * @param array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      },
	 *     installationCompleted: bool
	 * }|null $preloadedProjectData
	 * @return bool
	 */
	public function isProjectReady(string $identifier, ?array $preloadedProjectData = null): bool
	{
		return $this->projectCreateClient->isProjectReady($identifier, $preloadedProjectData);
	}

	public function changePackage(string $identifier, ?string $newPackage): void
	{
		$this->updateProjectClient->changePackage($identifier, $newPackage);
	}

	public function setMasterDomainToProject(string $identifier, string $domain): bool
	{
		if (!$this->isProjectReady($identifier))
		{
			return false;
		}
		return $this->updateProjectClient->setMasterDomainToProject($identifier, $domain);
	}

	public function refreshCertificate(string $domain): bool
	{
		return $this->updateProjectClient->refreshCertificate($domain);
	}

	/**
	 * @param string $userIdentifier
	 * @param string $redirectUri (portal/cms url)
	 * @return string
	 */
	public function getSingleSignOn(string $userIdentifier, string $redirectUri): string
	{
		return $this->oAuth2Api->getAuthorizationUrlForUser($userIdentifier, $redirectUri);
	}
}
