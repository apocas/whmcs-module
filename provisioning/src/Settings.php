<?php

namespace wnd\whmcs;

use WHMCS\Database\Capsule;

class Settings
{
	/** @var string */
	private $oauthUri;
	/** @var string */
	private $apiUrl;
	/** @var string */
	private $clientId;
	/** @var string  */
	private $clientSecret;


	public function __construct(
		string $oauthUri,
		string $apiUrl,
		string $clientId,
		string $clientSecret
	) {
		$this->oauthUri = $oauthUri;
		$this->apiUrl = $apiUrl;
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
	}


	public function getOauthUri(): string
	{
		return $this->oauthUri;
	}

	public function getApiUrl(): string
	{
		return $this->apiUrl;
	}

	public function getClientId(): string
	{
		return $this->clientId;
	}


	public function getClientSecret(): string
	{
		return $this->clientSecret;
	}
}
