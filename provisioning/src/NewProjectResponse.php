<?php

declare(strict_types=1);

namespace wnd\whmcs;

final class NewProjectResponse
{
	/** @var string */
	private $userHash;

	/** @var string */
	private $projectIdentifier;

	/** @var string */
	private $redirectUrl;

	public function toString(): string
	{
		return $this->projectIdentifier . " -- " . $this->userHash . " -- " . $this->redirectUrl;
	}

	public function getUserHash(): string
	{
		return $this->userHash;
	}

	/**
	 * @param string $userHash
	 * @return NewProjectResponse
	 */
	public function setUserHash(string $userHash): NewProjectResponse
	{
		$this->userHash = $userHash;

		return $this;
	}

	public function getProjectIdentifier(): string
	{
		return $this->projectIdentifier;
	}

	public function setProjectIdentifier(string $projectIdentifier): NewProjectResponse
	{
		$this->projectIdentifier = $projectIdentifier;

		return $this;
	}

	public function getRedirectUrl(): string
	{
		return $this->redirectUrl;
	}

	public function setRedirectUrl(string $redirectUrl): NewProjectResponse
	{
		$this->redirectUrl = $redirectUrl;

		return $this;
	}
}
