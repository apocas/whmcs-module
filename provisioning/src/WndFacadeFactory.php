<?php

namespace wnd\whmcs;

use wnd\whmcs\ApiClient\ApiAuthorizationService;
use wnd\whmcs\ApiClient\ApiProjectCreateClient;
use wnd\whmcs\ApiClient\ApiReadClient;
use wnd\whmcs\ApiClient\ApiUpdateProjectClient;
use wnd\whmcs\ApiClient\AuthorizedApiClient;
use wnd\whmcs\ApiClient\WebnodeOAuth2Service;
use wnd\whmcs\HttpClient\Client;

class WndFacadeFactory
{
	/** @var bool  */
	private $verify;

	public function __construct(bool $verify = true)
	{
		$this->verify = $verify;
	}

	private function createOAuth2Api(string $public, string $secret, string $oAuth2Url):WebnodeOAuth2Service
	{
		$client = new Client($oAuth2Url, $this->verify);
		return new WebnodeOAuth2Service($public, $secret, $client);
	}

	private function createAuthorizedApiClient(ApiAuthorizationService $authorizationService, string $apiUrl): AuthorizedApiClient
	{
		$client = new Client($apiUrl, $this->verify);
		return new AuthorizedApiClient($authorizationService, $client);
	}


	public function createFacade(string $public, string $secret, string $oAuth2Url, string $apiUrl): WebnodeFacade
	{
		$oAuth2Api = $this->createOAuth2Api($public, $secret, $oAuth2Url);
		$authorizeApiService = new ApiAuthorizationService($oAuth2Api);
		$api = $this->createAuthorizedApiClient($authorizeApiService, $apiUrl);

		$reader = new ApiReadClient($api);

		return new WebnodeFacade(
			$reader,
			new ApiProjectCreateClient($api, $reader),
			new ApiUpdateProjectClient($api, $reader),
			$oAuth2Api,
			$authorizeApiService
		);
	}
}
