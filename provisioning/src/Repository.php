<?php

namespace wnd\whmcs;

use WHMCS\Database\Capsule;

class Repository
{
	const PROJECTS_TABLE = 'mod_webnode_project';
	const QUEUE_TABLE = 'mod_webnode_queue';

	/** @var Settings|null  */
	private $settings = null;


	public function dropSchema(): void
	{
		/** @phpstan-ignore-next-line */
		\WHMCS\Database\Capsule::schema()->dropIfExists(self::PROJECTS_TABLE);
		/** @phpstan-ignore-next-line */
		\WHMCS\Database\Capsule::schema()->dropIfExists(self::QUEUE_TABLE);
	}


	public function getSettings(): Settings
	{
		if ($this->settings === null)
		{
			/** @phpstan-ignore-next-line */
			$results = Capsule::table('tbladdonmodules')->where('module', 'webnode')->get();

			$oauthUri = '';
			$apiUrl = '';
			$clientId = '';
			$clientSecret = '';
			foreach ($results as $result) {
				switch ($result->setting)
				{
					case 'oauth_uri':
						$oauthUri = $result->value;
						break;
					case 'api_url':
						$apiUrl = $result->value;
						break;
					case 'client_id':
						$clientId = $result->value;
						break;
					case 'client_secret':
						$clientSecret = $result->value;
						break;
					default:
						break;
				}
			}
			$this->settings = new Settings($oauthUri, $apiUrl, $clientId, $clientSecret);
		}
		return $this->settings;
	}

	/**
	 * @param string $projectIdentifier
	 * @param string $action
	 * @param array<string, mixed> $params
	 * @return void
	 */
	public function addToQueue(string $projectIdentifier, string $action, array $params = []): void
	{
		/** @phpstan-ignore-next-line */
		Capsule::table(self::QUEUE_TABLE)->insert(
			[
				'projectIdentifier' => $projectIdentifier,
				'action' => $action,
				'params' => json_encode($params),
			]
		);
	}

	/**
	 * @return object{ id: int, projectIdentifier: string, action: string, params: array<string, mixed>}|null
	 */
	public function processFromQueue(): ?object
	{
		/** @phpstan-ignore-next-line */
		$record = Capsule::table(self::QUEUE_TABLE)->orderBy('id', 'asc')->first();
		if (empty($record))
		{
			return null;
		}
		/** @phpstan-ignore-next-line */
		Capsule::table(self::QUEUE_TABLE)->where('id', '=', $record->id)->delete();
		$record->params = json_decode($record->params, true);
		return $record;
	}

	public function addProject(int $serviceId, string $projectIdentifier, string $adminUserIdentifier): void
	{
		/** @phpstan-ignore-next-line */
		Capsule::table(self::PROJECTS_TABLE)->insert(
			[
				'serviceId' => $serviceId,
				'projectIdentifier' => $projectIdentifier,
				'adminUserIdentifier' => $adminUserIdentifier,
			]
		);
	}

	public function getProjectInfo(int $serviceId): ?object
	{
		/** @phpstan-ignore-next-line */
		return Capsule::table(self::PROJECTS_TABLE)->where('serviceId', $serviceId)->first();
	}
}
