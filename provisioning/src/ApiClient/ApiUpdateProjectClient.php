<?php

namespace wnd\whmcs\ApiClient;

use wnd\whmcs\ApiClient\ApiReadClient;
use wnd\whmcs\ApiClient\AuthorizedApiClient;
use Exception;
use RuntimeException;
use wnd\whmcs\exceptions\ProjectNotFoundException;

class ApiUpdateProjectClient
{
	/** @var AuthorizedApiClient  */
	private $api;
	/** @var ApiReadClient  */
	private $readClient;

	public function __construct(AuthorizedApiClient $api, ApiReadClient $readClient)
	{
		$this->api = $api;
		$this->readClient = $readClient;
	}

	public function deleteProject(string $identifier): void
	{
		$this->api->request('DELETE', "/projects/$identifier");
	}

	/**
	 * For 30 days after project deletion, you are able to restore it
	 * @throws RuntimeException
	 */
	public function restoreSoftDeletedProject(string $identifier): void
	{
		$project = $this->readClient->findByIdentifier($identifier);
		if (empty($project))
		{
			throw new RuntimeException('Invalid project or project is deleted permanently');
		}
		if ($project['status'] !== 'DELETED')
		{
			throw new RuntimeException('Project is not soft deleted');
		}
		$this->update($identifier, 'RESTORED');
	}

	/**
	 * @throws RuntimeException
	 */
	public function setProjectEnabled(string $identifier, bool $enabled): void
	{
		$project = $this->readClient->findByIdentifier($identifier);
		if ($project === null)
		{
			throw new ProjectNotFoundException($identifier);
		}

		$status = $enabled ?'ENABLED': 'DISABLED';

		if ($project['status'] === $status)
		{
			return;
		}
		$this->update($identifier, $status);
	}

	/**
	 * @throws RuntimeException
	 */
	private function update(string $identifier, string $status): void
	{
		$response = $this->api->request('PATCH', "/projects/$identifier", [
			'status' => $status
		]);
		if ($response->getStatusCode() !== 204)
		{
			throw new RuntimeException(
				'Update could not be executed: ' . $response->getStatusCode() . ' ' . $response->getBody()
			);
		}
	}

	public function changePackage(string $identifier, ?string $newPackage): void
	{
		$currentPackages = $this->readClient->getProjectPackages($identifier);
		foreach ($currentPackages as $package)
		{
			if ($package['isActive'])
			{
				// package already set
				if ($newPackage === $package['identifier'])
				{
					return;
				}

				$this->api->request('DELETE', "/projects/$identifier/packages");
				break;
			}
		}

		if (!empty($newPackage))
		{
			$this->api->request('POST', "/projects/$identifier/packages", [
				'packageIdentifier' => $newPackage
			]);
		}
	}

	public function setMasterDomainToProject(string $identifier, string $domain): bool
	{
		try
		{
			$currentDomainProjectIdentifier = $this->readClient->getCurrentProjectForDomain($domain);
			if ($currentDomainProjectIdentifier)
			{
				if ($identifier === $currentDomainProjectIdentifier)
				{
					$this->api->request('PATCH', "/domains/$domain");

					return true;
				}
				// remove domain from old project
				$this->api->request(
					'DELETE',
					"/projects/$currentDomainProjectIdentifier/domains/$domain"
				);
			}
			$this->api->request('POST', "/projects/$identifier/domains", [
				'domainName' => $domain
			]);
			$this->api->request('PATCH', "/domains/$domain");

			return true;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	public function refreshCertificate(string $domain): bool
	{
		try
		{
			$this->api->request('POST', "/domains/$domain/certificate");
			return true;
		}
		catch (Exception $e)
		{
			return false;
		}
	}
}
