<?php

namespace wnd\whmcs\ApiClient;

use wnd\whmcs\ApiClient\AuthorizedApiClient;
use InvalidArgumentException;
use wnd\whmcs\HttpClient\Response;

class ApiReadClient
{
	/** @var AuthorizedApiClient  */
	private $api;

	public function __construct(AuthorizedApiClient $api)
	{
		$this->api = $api;
	}

	/**
	 * @param string $userIdentifier
	 * @return  array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      }
	 * }[]|null
	 * @throws InvalidArgumentException
	 */
	public function findByUserIdentifier(string $userIdentifier): ?array
	{
		return $this->getResponseData($this->api->request('GET', "/users/$userIdentifier/projects"));
	}

	/**
	 * @param string $identifier
	 * @return array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      },
	 *     installationCompleted: bool
	 * }|null
	 */
	public function findByIdentifier(string $identifier): ?array
	{
		/** @var array{
		 *		identifier: string,
		 * 		projectUrl: string,
		 *      domainName: string,
		 *      cmsUrl: string,
		 *      adminUserIdentifier: string,
		 *      portalUrl: string,
		 *      serverIpAddress: string,
		 *      status: string,
		 *      replyToEmail: string,
		 *      adminEmail: string,
		 *      "limits": array{
		 *      	bandwidthMaximum: float,
		 *      	bandwidthUsage: float,
		 *      	storageMaximum: int,
		 *      	storageUsage: int
		 *      },
		 *     installationCompleted: bool
		 * }|null $data */
		$data = $this->getResponseData($this->api->request('GET', "/projects/$identifier"));
		return $data;
	}

	/**
	 * @param string $identifier
	 * @return array{identifier:string, isActive: bool}[]
	 */
	public function getProjectPackages(string $identifier): array
	{
		/** @var array{identifier:string, isActive: bool}[]|null $data */
		$data = $this->getResponseData($this->api->request('GET', "/projects/$identifier/packages"));
		return $data ?? [];
	}

	/**
	 * @param string $identifier
	 * @return array{
	 *      domainName: string,
	 *      isHttps: bool,
	 *      createDate: string,
	 *      isMasterDomain: bool,
	 *      projectIdentifier:string,
	 *      userIdentifier: string
	 * }[]
	 */
	public function getProjectDomains(string $identifier): array
	{
		/** @var array{
		 *      domainName: string,
		 *      isHttps: bool,
		 *      createDate: string,
		 *      isMasterDomain: bool,
		 *      projectIdentifier:string,
		 *      userIdentifier: string
		 * }[]|null $data */
		$data = $this->getResponseData($this->api->request('GET', "/projects/$identifier/domains"));
		return $data ?? [];
	}


	public function getCurrentProjectForDomain(string $domain): ?string
	{
		$data = $this->getResponseData($this->api->request('GET', "/domains/$domain"));
		if ($data)
		{
			return $data['projectIdentifier'];
		}
		return null;
	}

	/**
	 * @param Response $response
	 * @return array<string|int, mixed>|null
	 */
	private function getResponseData(Response $response): ?array
	{
		if ($response->getStatusCode() < 300)
		{
			return json_decode($response->getBody(), true)['data'];
		}
		return null;
	}
}
