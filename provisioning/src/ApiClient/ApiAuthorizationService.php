<?php

declare(strict_types=1);

namespace wnd\whmcs\ApiClient;

/**
 * Service for providing API authorization header
 */
final class ApiAuthorizationService
{
	/** @var string|null  */
	private $token = null;

	/** @var WebnodeOAuth2Service */
	private $oAuth2Api;

	public function __construct(WebnodeOAuth2Service $oAuth2Api)
	{
		$this->oAuth2Api = $oAuth2Api;
	}


	public function getAuthorizationHeader(): string
	{
		$token = $this->getAccessToken();

		return "Bearer $token";
	}

	private function getAccessToken(): string
	{
		if (!$this->token)
		{
			$this->token = $this->oAuth2Api->clientCredentialsGrant();
		}

		return $this->token;
	}

	public function revokeAccessToken(): void
	{
		if ($this->token)
		{
			$this->oAuth2Api->revokeToken($this->token);
			$this->token = null;
		}
	}
}
