<?php

namespace wnd\whmcs\ApiClient;

use wnd\whmcs\exceptions\AccessDeniedException;
use wnd\whmcs\exceptions\InvalidRequestException;
use wnd\whmcs\exceptions\OAuth2Exception;
use wnd\whmcs\exceptions\UnexpectedResponseException;
use wnd\whmcs\HttpClient\Client;
use wnd\whmcs\HttpClient\Response;

/**
 * This is replacement for webnode/oauth2-client, which only supports php ^7.4 and thus is not usable in WHMCS 7
 */
class WebnodeOAuth2Service
{
	private const REVOKE_PATH = '/revoke';
	private const ACCESS_TOKEN_PATH = '/access_token';
	private const AUTHORIZE_PATH = '/authorize';

	/** @var string */
	private $clientId;

	/** @var string */
	private $clientSecret;

	/** @var Client */
	private $httpClient;

	public function __construct(string $clientId, string $clientSecret, Client $httpClient)
	{
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
		$this->httpClient = $httpClient;
	}

	/**
	 * @param string $userIdentifier
	 * @param string $redirectUri
	 * @param string|null $appRedirectUri
	 * @param string|null $stateRedirectUri
	 * @param string[] $scope
	 * @param array<string, mixed>|null $sessionData
	 * @return string
	 */
	public function getAuthorizationUrlForUser(
		string $userIdentifier,
		string $redirectUri,
		?string $appRedirectUri = null,
		?string $stateRedirectUri = null,
		array $scope = ['basic'],
		?array $sessionData = null
	): string {
		$data = [
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
			'user_identifier' => $userIdentifier,
			'redirect_uri' => $redirectUri,
			'scope' => implode(' ', $scope),
		];
		if ($sessionData)
		{
			$data['session_data'] = json_encode($sessionData);
		}
		if ($appRedirectUri)
		{
			$data['app_uri'] = $appRedirectUri;
		}
		if ($stateRedirectUri)
		{
			$data['dest_uri'] = $stateRedirectUri;
		}


		$response = $this->httpClient->request('POST', self::AUTHORIZE_PATH, $data);

		$body = $this->getValidJsonResponseData($response);

		if (empty($body['authorizationUrl']))
		{
			throw new UnexpectedResponseException('Response does not contain authorizationUrl');
		}
		return $body['authorizationUrl'];
	}

	/**
	 * @param string[] $scope
	 * @return string
	 */
	public function clientCredentialsGrant(array $scope = ['basic']): string
	{
		$response = $this->httpClient->request('POST', self::ACCESS_TOKEN_PATH, [
			'grant_type' => 'client_credentials',
			'scope'   => implode(' ', $scope),
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
		]);

		$body = $this->getValidJsonResponseData($response);
		if (empty($body['token_type']) || $body['token_type'] !== 'Bearer' || empty($body['access_token']))
		{
			throw new UnexpectedResponseException('Response is Not Valid Bearer Token Response');
		}

		return $body['access_token'];
	}

	public function revokeToken(string $accessToken): void
	{

		$response = $this->httpClient->request('POST', self::REVOKE_PATH, [
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
			'access_token' => $accessToken,
		]);

		$body = $this->getValidJsonResponseData($response);

		if (!($body['success'] ?? false))
		{
			throw new UnexpectedResponseException('Could not revoke tokens, response: ' . print_r($body, true));
		}
	}

	/**
	 * @param Response $response
	 * @return array<string, mixed>
	 */
	public function getValidJsonResponseData(Response $response): array
	{
		$bodyStr = $response->getBody();
		$body = json_decode($bodyStr, true);
		if (json_last_error() !== JSON_ERROR_NONE)
		{
			throw new UnexpectedResponseException(
				'Invalid response: ' . substr($bodyStr, 0, 100)
			);
		}

		if ($response->getStatusCode() === 401)
		{
			throw (new AccessDeniedException($body['message']))->setErrorIdentifier($body['error']);
		}
		elseif ($response->getStatusCode() >= 400 && $response->getStatusCode() < 500)
		{
			throw (new InvalidRequestException($body['message']))->setErrorIdentifier($body['error']);
		}
		elseif ($response->getStatusCode() !== 200)
		{
			throw new UnexpectedResponseException(
				'Invalid status code '.$response->getStatusCode().' with data: '. json_encode($body)
			);
		}

		return $body;
	}
}
