<?php

namespace wnd\whmcs\ApiClient;

use Exception;
use InvalidArgumentException;
use RuntimeException;
use wnd\whmcs\NewProjectResponse;

class ApiProjectCreateClient
{
	/** @var AuthorizedApiClient */
	private $api;
	/** @var ApiReadClient */
	private $readClient;

	public function __construct(AuthorizedApiClient $api, ApiReadClient $readClient)
	{
		$this->api = $api;
		$this->readClient = $readClient;
	}

	/**
	 * @param string $userEmail
	 * @param string|null $identifier
	 * @return NewProjectResponse
	 * @throws InvalidArgumentException
	 * @throws Exception
	 *
	 */
	public function create(string $userEmail, string $identifier = null): NewProjectResponse
	{
		$identifier = $identifier ?? 'w' . bin2hex(random_bytes(10));
		// call an API
		$response = $this->api->request('POST', '/projects', [
			'projectIdentifierHint' => $identifier,
			'language' => 'en',
			'email' => $userEmail,
			'replyToEmail' => '',
		]);
		$responseData = json_decode($response->getBody(), true);

		return (new NewProjectResponse())
			->setUserHash($responseData['data']['adminUserIdentifier'])
			->setProjectIdentifier($identifier)
			->setRedirectUrl($responseData['data']['cmsUrl']);
	}

	/**
	 * @param string $identifier
	 * @param array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      },
	 *     installationCompleted: ?bool
	 * }|null $preloadedProjectData
	 * @return bool
	 */
	public function isProjectReady(string $identifier, ?array $preloadedProjectData = null): bool
	{
		if ($preloadedProjectData && isset($preloadedProjectData['installationCompleted']))
		{
			return $preloadedProjectData['installationCompleted'];
		}

		$data = $this->readClient->findByIdentifier($identifier);
		if (empty($data))
		{
			return false;
		}
		return $data['installationCompleted'];
	}

	/**
	 * @param string $identifier
	 * @param int $timeout (in ms)
	 * @param int $step (in ms)
	 * @return void
	 * @throws RuntimeException
	 */
	public function waitForProjectReady(string $identifier, int $timeout = 60000, int $step = 250): void
	{
		$timeout *=1000;
		$step *=1000;

		$msLeft = $timeout;

		while (!$this->isProjectReady($identifier))
		{
			if ($msLeft <= 0)
			{
				throw new RuntimeException('Project-ready wait timeout');
			}
			$msLeft -= $step;
			usleep($step);
		}
	}
}
