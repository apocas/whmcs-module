<?php

namespace wnd\whmcs\ApiClient;

use wnd\whmcs\HttpClient\Client;
use wnd\whmcs\HttpClient\Response;

class AuthorizedApiClient
{
	/** @var ApiAuthorizationService  */
	private $authService;

	/** @var Client  */
	private $httpClient;

	public function __construct(ApiAuthorizationService $authService, Client $httpClient)
	{
		$this->authService = $authService;
		$this->httpClient = $httpClient;
	}

	/**
	 * @param string $method
	 * @param string $uri
	 * @param array<string, mixed> $data
	 * @return Response
	 */
	public function request(string $method, string $uri, array $data = []): Response
	{
		return $this->httpClient->request($method, $uri, $data, [
			'Authorization: ' . $this->authService->getAuthorizationHeader()
		]);
	}
}
