<?php

namespace wnd\whmcs\exceptions;

class OAuth2Exception extends \RuntimeException
{
	/** @var string */
	private $errorIdentifier;

	public function setErrorIdentifier(string $errorIdentifier): OAuth2Exception
	{
		$this->errorIdentifier = $errorIdentifier;

		return $this;
	}

	public function getErrorIdentifier(): string
	{
		return $this->errorIdentifier;
	}
}
