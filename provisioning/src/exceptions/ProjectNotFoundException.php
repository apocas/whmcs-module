<?php

namespace wnd\whmcs\exceptions;

use RuntimeException;

class ProjectNotFoundException extends RuntimeException
{

}
