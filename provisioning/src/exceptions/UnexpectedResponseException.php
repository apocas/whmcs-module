<?php

namespace wnd\whmcs\exceptions;

use wnd\whmcs\exceptions\OAuth2Exception;

class UnexpectedResponseException extends OAuth2Exception
{

}
