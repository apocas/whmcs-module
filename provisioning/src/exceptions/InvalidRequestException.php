<?php

namespace wnd\whmcs\exceptions;

use wnd\whmcs\exceptions\OAuth2Exception;

class InvalidRequestException extends OAuth2Exception
{

}
