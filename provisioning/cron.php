<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/../../../vendor/autoload.php';
require_once __DIR__ . '/../../../init.php';

use wnd\whmcs\WndFacadeFactory;
use wnd\whmcs\Repository;

$public = $argv[1];
$secret = $argv[2];
$oAuthUrl = $argv[3];
$apiUrl = $argv[4];
$count = $argv[5] ?? 1;

$repositoryInstance = new Repository();
$webnodeFacade = (new WndFacadeFactory())->createFacade($public, $secret, $oAuthUrl, $apiUrl);

while ($count > 0)
{
	$count--;
	$data = $repositoryInstance->processFromQueue();
	if(!$webnodeFacade->isProjectReady($data->projectIdentifier))
	{
		$repositoryInstance->addToQueue($data->projectIdentifier, $data->action, $data->params);
		continue;
	}
	try
	{
		switch ($data->action)
		{
			case 'setDomain':
				$webnodeFacade->setMasterDomainToProject($data->projectIdentifier, $data->params['domain']);
				break;
			default:
				return;
		}
	}
	catch (\Throwable $exception)
	{
		error_log(print_r($exception, true));
		fwrite(STDERR, 'action failed');
		$repositoryInstance->addToQueue($data->projectIdentifier, $data->action, $data->params);
	}
}