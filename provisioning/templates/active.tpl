<h3> Sitebuilder</h3>
<div class="row">
	<div class="col-sm-5 text-right">
		<strong>Project IP</strong>
	</div>
	<div class="col-sm-7 text-left">
		{$ip}
	</div>
</div>
<h4>Required Configuration</h4>
<div class="row">
	<table class="table">
		<thead>
			<tr>
				<th>Type</th>
				<th>Name</th>
				<th>Value</th>
			</tr>
		</thead>
		<tbody class="text-left">
			<tr>
				<td>A</td>
				<td>{$domain}</td>
				<td>{$ip}</td>
			</tr>
			<tr>
				<td>A</td>
				<td>*.{$domain}</td>
				<td>{$ip}</td>
			</tr>
		</tbody>
	</table>
	{if not $domainAssigned }
		<p style="font-weight: bold;">Domain is not set to project. Please setup DNS records above and assign domain.</p>
		<a href="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=assignDomain" class="btn btn-primary">Assign Domain</a>
	{elseif not $httpsActive }
		<p style="font-weight: bold;">Domain is has not active HTTPS. Please setup DNS records above and refresh certificate.</p>
		<a href="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=refreshCert" class="btn btn-primary">Refresh certificate</a>
    {/if}
</div>

<h4>Go to sitebuilder edit:</h4>
<p class="text-center">
	<a href="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=ssoEdit" class="btn btn-primary">Edit</a>
</p>