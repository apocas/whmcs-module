<?php

// This is the bootstrap for PHPUnit testing.

if (!defined('WHMCS')) {
	define('WHMCS', true);
}

require_once __DIR__ . '/../vendor/autoload.php';
