<?php

namespace wnd\whmcs\unit\integration;

use wnd\whmcs\HttpClient\Client;

class SsoTest extends AbstractApiIntegration
{
	public static function setUpBeforeClass(): void
	{
		self::staticSetup();
	}

	public function testSSO(): void
	{
		$projects = self::$webnodeFacade->findByUserIdentifier(self::$config['testData']['user']['hash']);
		$this->assertNotNull($projects);
		$selectedProject = null;
		foreach ($projects as $project)
		{
			if ($project['identifier'] === self::$config['testData']['projects']['basic']['identifier'])
			{
				$selectedProject = $project;
			}
		}
		$this->assertNotEmpty($selectedProject);

		$cmsUrl = $selectedProject['cmsUrl'];
		$this->assertNotEmpty($cmsUrl);

		$redirectUrl = self::$webnodeFacade->getSingleSignOn(self::$config['testData']['user']['hash'], $cmsUrl);
		$guzzle = new Client('', false);
		$oauth2Response = $guzzle->request('GET', $redirectUrl);
		$this->assertEquals(302, $oauth2Response->getStatusCode());
		$this->assertEquals("$cmsUrl?success=1&state=1", $oauth2Response->getHeader('location'));
	}
}
