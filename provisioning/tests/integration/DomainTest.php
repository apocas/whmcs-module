<?php

namespace wnd\whmcs\unit\integration;

class DomainTest extends AbstractApiIntegration
{

	public static function setUpBeforeClass(): void
	{
		self::staticSetup();
	}

	public function testMasterDomain(): void
	{
		$identifier = self::$config['testData']['projects']['domain']['identifier'];
		$domain1 = self::$config['testData']['projects']['domain']['domain1'];
		$domain2 = self::$config['testData']['projects']['domain']['domain2'];

		$nextDomain = $this->getMasterDomain($identifier) === $domain1 ? $domain2 : $domain1;
		$this->assertTrue(self::$webnodeFacade->setMasterDomainToProject($identifier, $nextDomain));
		$this->assertEquals($nextDomain, $this->getMasterDomain($identifier));
	}


	private function getMasterDomain(string $identifier): string
	{
		$projectDomains = self::$webnodeFacade->getProjectDomains($identifier);
		foreach ($projectDomains as $domain)
		{
			if ($domain['isMasterDomain'])
			{
				return $domain['domainName'];
			}
		}
		return '';
	}
}
