<?php

namespace wnd\whmcs\unit\integration;

class PackageTest extends AbstractApiIntegration
{

	public static function setUpBeforeClass(): void
	{
		self::staticSetup();
	}

	public function testPackageDomain(): void
	{
		$identifier = self::$config['testData']['projects']['package']['identifier'];
		$package1 = self::$config['testData']['projects']['package']['package1'];
		$package2 = self::$config['testData']['projects']['package']['package2'];



		$nextPackage = $this->getActivePackage($identifier) === $package1 ? $package2 : $package1;
		self::$webnodeFacade->changePackage($identifier, $nextPackage);
		$this->assertEquals($nextPackage, $this->getActivePackage($identifier));
	}

	private function getActivePackage(string $identifier): string
	{
		$packages = self::$webnodeFacade->getProjectPackages($identifier);
		foreach ($packages as $domain)
		{
			if ($domain['isActive'])
			{
				return $domain['identifier'];
			}
		}
		return '';
	}
}
