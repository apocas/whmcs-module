<?php

namespace wnd\whmcs\unit\integration;

class FacadeReadTest extends AbstractApiIntegration
{

	public static function setUpBeforeClass(): void
	{
		self::staticSetup();
	}


	public function testFindProject(): void
	{
		$projects = self::$webnodeFacade->findByIdentifier(self::$config['testData']['projects']['basic']['identifier']);
		$this->assertTrue(is_array($projects));
		$this->assertNotEmpty($projects);
	}

	public function testFindProjects(): void
	{
		$projects = self::$webnodeFacade->findByUserIdentifier(self::$config['testData']['user']['hash']);
		$this->assertNotNull($projects);

		foreach (self::$config['testData']['projects'] as $testData)
		{
			$this->assertTrue($this->isProjectInList($projects, $testData['identifier']));
		}
	}

	/**
	 * @param array{
	 *		identifier: string,
	 * 		projectUrl: string,
	 *      domainName: string,
	 *      cmsUrl: string,
	 *      adminUserIdentifier: string,
	 *      portalUrl: string,
	 *      serverIpAddress: string,
	 *      status: string,
	 *      replyToEmail: string,
	 *      adminEmail: string,
	 *      "limits": array{
	 *      	bandwidthMaximum: float,
	 *      	bandwidthUsage: float,
	 *      	storageMaximum: int,
	 *      	storageUsage: int
	 *      }
	 * }[] $list
	 * @param string $identifier
	 * @return bool
	 */
	public function isProjectInList(array $list, string $identifier): bool
	{
		foreach ($list as $data)
		{
			if ($data['identifier'] === $identifier)
			{
				return true;
			}
		}
		return false;
	}
}
