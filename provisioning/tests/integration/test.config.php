<?php
$config = include(__DIR__ . '/../../config.php');

return array_merge($config, [
	'credentials' => [
		'public' => getenv('OAUTH2_PUBLIC') ?: $_ENV['OAUTH2_PUBLIC'] ?? '',
		'secret' => getenv('OAUTH2_SECRET') ?: $_ENV['OAUTH2_SECRET'] ?? '',
	],
	'testData' => [
		'user' => [
			'email' => 'test-user@rubicus.cz',
			'hash' => 'cd35245c151fbbf3dbae33003a2e7f8604b2e953',
		],
		'projects' => [
			'basic' => [
				'identifier' => 'w16c950007a21dcf243c7',
			],
			'domain' => [
				'identifier' => 'whmcs-domain-test',
				'domain1' => 'whmcs-1086.bures.app',
				'domain2' => 'whmcs-1086-2.bures.app',
			],
			'package' => [
				'identifier' => 'whmcs-package-test',
				'package1' => 'limited',
				'package2' => 'mini',
			],
		],
	],
	'servers' => [
		'oauth2' => 'https://oauth2.webnode.com',
		'api' => 'https://api-gateway.webnode.com',
	],
]);
