<?php

namespace wnd\whmcs\unit\integration;

use Dotenv\Dotenv;
use Exception;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use wnd\whmcs\WebnodeFacade;
use wnd\whmcs\WndFacadeFactory;

abstract class AbstractApiIntegration extends TestCase
{

	/** @var WebnodeFacade  */
	protected static $webnodeFacade;

	/** @var array<string, mixed>  */
	protected static $config;

	protected static function staticSetup(): void
	{
		$dotenv = Dotenv::createImmutable(__DIR__, '.env');
		$dotenv->safeLoad();

		if (!file_exists(__DIR__ . '/test.config.php'))
		{
			throw new RuntimeException('test.config.php is missing, please read README.md');
		}
		self::$config = include(__DIR__ . '/test.config.php');

		self::$webnodeFacade = (new WndFacadeFactory(false))->createFacade(
			self::$config['credentials']['public'],
			self::$config['credentials']['secret'],
			self::$config['servers']['oauth2'],
			self::$config['servers']['api']
		);
	}

	public static function tearDownAfterClass(): void
	{
		self::$webnodeFacade->clean();
	}

	/**
	 * @param callable $reloadData
	 * @param callable $checkData
	 * @param int $timeout
	 * @param int $timeStep
	 * @return null|mixed
	 */
	protected function waitFor(callable $reloadData, callable $checkData, int $timeout = 10, int $timeStep = 1)
	{
		$remainingTime = $timeout;
		while ($remainingTime > 0)
		{
			$data = $reloadData();
			if ($checkData($data))
			{
				return $data;
			}
			$remainingTime-=$timeStep;
			sleep($timeStep);
		}
		return null;
	}

	/**
	 * @throws Exception
	 */
	protected function getUniqueName(): string
	{
		return 'test-whmcs-' . bin2hex(random_bytes(1)) . str_replace(['0.', ' '], '-', microtime());
	}
}
