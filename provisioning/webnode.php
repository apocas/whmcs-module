<?php

use wnd\whmcs\exceptions\ProjectNotFoundException;
use wnd\whmcs\WebnodeFacade;
use wnd\whmcs\WndFacadeFactory;
use wnd\whmcs\Repository;

if (!defined("WHMCS"))
{
	die("This file cannot be accessed directly");
}

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related abilities and
 * settings.
 *
 * @see https://developers.whmcs.com/provisioning-modules/meta-data-params/
 *
 * @return array
 */
function webnode_MetaData()
{
	return array(
		'DisplayName' => 'Team.blue Webnode',
		'APIVersion' => '1.1',
		'RequiresServer' => false,
		'DefaultNonSSLPort' => '80',
		'DefaultSSLPort' => '443',
		'ServiceSingleSignOnLabel' => 'Login to web administration as User',
	);
}


/**
 * Define product configuration options.
 *
 * The values you return here define the configuration options that are
 * presented to a user when configuring a product for use with the module. These
 * values are then made available in all module function calls with the key name
 * configoptionX - with X being the index number of the field from 1 to 24.
 *
 * You can specify up to 24 parameters, with field types:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each and their possible configuration parameters are provided in
 * this sample function.
 *
 * @see https://developers.whmcs.com/provisioning-modules/config-options/
 *
 * @return array
 */
function webnode_ConfigOptions()
{
	$config = include(__DIR__ . '/config.php');
  	return array(
	  'package' => array(
		  'Type' => 'text',
		  'Size' => '25',
		  'Default' => '',
		  'Description' => 'Package',
		  "FriendlyName" => "Package",
	  )
  );
}

function createFacade(array $params): WebnodeFacade
{
	$settings = getRepository()->getSettings();
	return (new WndFacadeFactory())->createFacade(
		$settings->getClientId(),
		$settings->getClientSecret(),
		$settings->getOauthUri(),
		$settings->getApiUrl()
	);
}
$repositoryInstance = null;
function getRepository(): Repository
{
	global $repositoryInstance;
	if ($repositoryInstance === null)
	{
		$repositoryInstance = new Repository();
	}
	return $repositoryInstance;
}

function loadProjectInfo(int $serviceId): object
{
	$projectInfo = getRepository()->getProjectInfo($serviceId);
	if (!$projectInfo)
	{
		throw new ProjectNotFoundException();
	}
	return $projectInfo;
}


function loadProjectData(WebnodeFacade $webnodeFacade, int $serviceId): array
{
	$projectInfo = loadProjectInfo($serviceId);
	$project = $webnodeFacade->findByIdentifier($projectInfo->projectIdentifier);
	if (!$project)
	{
		throw new ProjectNotFoundException();
	}
	return $project;

}


/**
 * Provision a new instance of a product/service.
 *
 * Attempt to provision a new instance of a given product/service. This is
 * called any time provisioning is requested inside of WHMCS. Depending upon the
 * configuration, this can be any of:
 * * When a new order is placed
 * * When an invoice for a new order is paid
 * * Upon manual request by an admin user
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_CreateAccount(array $params)
{
	try
	{
		$webnodeFacade = createFacade($params);
		$project = $webnodeFacade->create($params['clientsdetails']['email']);

		$webnodeFacade->changePackage($project->getProjectIdentifier(), $params['configoption1']);
		$repo = getRepository();
		$repo->addProject(
			$params['serviceid'],
			$project->getProjectIdentifier(),
			$project->getUserHash()
		);

		$webnodeFacade->clean();
		return 'success';
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Suspend an instance of a product/service.
 *
 * Called when a suspension is requested. This is invoked automatically by WHMCS
 * when a product becomes overdue on payment or can be called manually by admin
 * user.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_SuspendAccount(array $params)
{
	try
	{
		$webnodeFacade = createFacade($params);
		$project = loadProjectData($webnodeFacade, $params['serviceid']);
		$webnodeFacade->setProjectEnabled($project['identifier'], false);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Un-suspend instance of a product/service.
 *
 * Called when an un-suspension is requested. This is invoked
 * automatically upon payment of an overdue invoice for a product, or
 * can be called manually by admin user.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_UnsuspendAccount(array $params)
{
	try
	{
		$webnodeFacade = createFacade($params);
		$project = loadProjectData($webnodeFacade, $params['serviceid']);
		$webnodeFacade->setProjectEnabled($project['identifier'], true);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Terminate instance of a product/service.
 *
 * Called when a termination is requested. This can be invoked automatically for
 * overdue products if enabled, or requested manually by an admin user.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_TerminateAccount(array $params)
{
	try
	{

		$webnodeFacade = createFacade($params);
		$project = loadProjectData($webnodeFacade, $params['serviceid']);
		$webnodeFacade->deleteProject($project['identifier']);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Upgrade or downgrade an instance of a product/service.
 *
 * Called to apply any change in product assignment or parameters. It
 * is called to provision upgrade or downgrade orders, as well as being
 * able to be invoked manually by an admin user.
 *
 * This same function is called for upgrades and downgrades of both
 * products and configurable options.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_ChangePackage(array $params)
{
	try
	{
		$webnodeFacade = createFacade($params);
		$project = loadProjectData($webnodeFacade, $params['serviceid']);
		$webnodeFacade->changePackage($project['identifier'], $params['configoption1']);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Renew an instance of a product/service.
 *
 * Attempt to renew an existing instance of a given product/service. This is
 * called any time a product/service invoice has been paid.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_Renew(array $params)
{
	return 'success';
}

/**
 * Test connection with the given server parameters.
 *
 * Allows an admin user to verify that an API connection can be
 * successfully made with the given configuration parameters for a
 * server.
 *
 * When defined in a module, a Test Connection button will appear
 * alongside the Server Type dropdown when adding or editing an
 * existing server.
 *
 * @param array $params common module parameters
 *
 * @return array
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_TestConnection(array $params)
{
	try
	{
		// todo implement
		// Call the service's connection test function.

		$success = true;
		$errorMsg = '';
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		$success = false;
		$errorMsg = $e->getMessage();
	}

	return array(
		'success' => $success,
		'error' => $errorMsg,
	);
}

/**
 * Perform single sign-on for a given instance of a product/service.
 *
 * Called when single sign-on is requested for an instance of a product/service.
 *
 * When successful, returns a URL to which the user should be redirected.
 *
 * @param array $params common module parameters
 *
 * @return array
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_ServiceSingleSignOn(array $params)
{
	try
	{
		$webnodeFacade = createFacade($params);
		$projectInfo = loadProjectInfo($params['serviceId']);
		$project = $webnodeFacade->findByIdentifier($projectInfo->projectIdentifier);
		if (!$webnodeFacade->isProjectReady($projectInfo->projectIdentifier, $project))
		{
			return array(
				'success' => false,
				'errorMsg' => 'Project not fully installed',
			);
		}

		if (!$project)
		{
			throw new ProjectNotFoundException();
		}

		return array(
			'success' => true,
			'redirectTo' => $webnodeFacade->getSingleSignOn($projectInfo->adminUserIdentifier, $project['portalUrl']),
		);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return array(
			'success' => false,
			'errorMsg' => $e->getMessage(),
		);
	}
}

function webnode_ClientArea(array $params)
{
	$webnodeFacade = createFacade($params);
	$projectInfo = loadProjectInfo($params['serviceid']);
	$project = $webnodeFacade->findByIdentifier($projectInfo->projectIdentifier);
	if (!$webnodeFacade->isProjectReady($projectInfo->projectIdentifier, $project))
	{
		return [
			'templatefile' => 'templates/inactive.tpl',
			'vars' => [],
		];
	}

	if(isset($_GET['userAction']))
	{
		switch ($_GET['userAction'])
		{
			// solution copied from DUDA, it's ugly, but I can't make WHMCS to call webnode_ServiceSingleSignOn function
			case 'ssoEdit':
				header('Location: ' . $webnodeFacade->getSingleSignOn($projectInfo->adminUserIdentifier, $project['portalUrl']));
				http_response_code(302);
				die();
			case 'assignDomain':
				$webnodeFacade->setMasterDomainToProject($projectInfo->projectIdentifier, $params['domain']);
				$project = $webnodeFacade->findByIdentifier($projectInfo->projectIdentifier);
				break;
			case 'refreshCert':
				$webnodeFacade->refreshCertificate($params['domain']);
				$project = $webnodeFacade->findByIdentifier($projectInfo->projectIdentifier);
				break;
			default:
				break;
		}
	}

	$domainAssigned = false;
	$httpsActive = false;

	foreach ($webnodeFacade->getProjectDomains($projectInfo->projectIdentifier) as $domain)
	{
		if ($domain['domainName'] === $params['domain'])
		{
			$domainAssigned = $domain['isMasterDomain'];
			$httpsActive = $domain['isHttps'];
			break;
		}
	}

	return [
		'templatefile' => 'templates/active.tpl',
		'vars' => [
			'ip' => $project['serverIpAddress'],
			'domain' => $params['domain'],
			'domainAssigned' => $domainAssigned,
			'httpsActive' => $httpsActive,
			'serviceId' => $params['serviceid'],
		],
	];
}